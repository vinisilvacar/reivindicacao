package model;

public class Usuario {

	//Atributos
	String nome;
	String Cpf;
	String Local;
	String email;
	
	//Métodos
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	public String getCpf() {
		return Cpf;
	}
	public void setCpf(String cpf) {
		Cpf = cpf;
	}

	
	public String getLocal() {
		return Local;
	}
	public void setLocal(String local) {
		Local = local;
	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
		
}
